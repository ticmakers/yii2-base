<?php
namespace ticmakers\base\base;

use yii\web\View as YiiView;

class View extends YiiView
{
    public function afterRender($viewFile, $params, &$output)
    {
        $output = str_replace('{title-grid}', $this->title, $output);
    }
}
