<?php

namespace ticmakers\base\base;

use ticmakers\base\helpers\Message;
use ticmakers\base\helpers\UI;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller as ControllerBase;
use yii\web\Response;
use ticmakers\base\helpers\Constants as C;
use ticmakers\base\actions\DepDropAction;

/**
 * Controlador base para todas las CRUD generales
 *
 * @package ticmakers
 * @subpackage base
 * @category Base
 *
 * @property string $searchModel Ruta del modelo para la búsqueda.
 * @property string $model Ruta del modelo principal.
 * @property string $layout PATH del layout utilizado por defecto.
 *
 * @author  Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Controller extends ControllerBase
{
    public $searchModel;
    public $model;
    public $layout = '@theme/views/layouts/main';
    public $beforeSearchIndex;
    public $beforeRenderIndex;
    public $actionsConfig = [];
    public $depDropActionClass;

    public function actions()
    {
        if (empty($this->depDropActionClass)) {
            $this->depDropActionClass = DepDropAction::class;
        }
        $parentActions = parent::actions();
        $depDropActions = [];

        if ($this->model) {
            $modelObject = \Yii::createObject($this->model);
            if ($modelObject) {
                foreach ($modelObject->depDropDependencies() as $actionId => $dDropConfig) {
                    if (!isset($dDropConfig[C::CLASS_NAME])) {
                        $dDropConfig[C::CLASS_NAME] = $this->depDropActionClass;
                    }
                    $depDropActions[$actionId] = $dDropConfig;
                }
            }
        }
        return array_merge($this->actionsConfig, $parentActions, $depDropActions);
    }

    /**
     * @todo Documentar
     *
     */
    public function beforeSearchIndex(&$searchModel)
    {
        if ($this->beforeSearchIndex) {
            call_user_func_array($this->beforeSearchIndex, [&$searchModel]);
        }
    }
    /**
     * @todo Documentar
     *
     */
    public function beforeRenderIndex(&$viewFile, &$params)
    {
        if ($this->beforeRenderIndex) {
            call_user_func_array($this->beforeRenderIndex, [&$viewFile, &$params]);
        }
    }

    /**
     * Método para sobreescribir comportamientos antes de las acciones
     *
     * @param \yii\base\Action $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        Yii::$container->set(
            'kartik\grid\GridView',
            UI::getDefaultConfigGridView($this->getTitleReportExport())
        );
        Yii::$container->set(
            'kartik\dynagrid\DynaGrid',
            UI::getDefaultConfigDynaGrid()
        );
        Yii::$container->set(
            'kartik\widgets\TimePicker',
            UI::getDefaultConfigTimePicker()
        );
        Yii::$container->set(
            'kartik\time\TimePicker',
            UI::getDefaultConfigTimePicker()
        );
        Yii::$container->set(
            'kartik\widgets\DatePicker',
            UI::getDefaultConfigDatePicker()
        );
        Yii::$container->set(
            'kartik\date\DatePicker',
            UI::getDefaultConfigDatePicker()
        );
        Yii::$container->set(
            'kartik\widgets\DateTimePicker',
            UI::getDefaultConfigDateTimePicker()
        );
        Yii::$container->set(
            'kartik\datetime\DateTimePicker',
            UI::getDefaultConfigDateTimePicker()
        );
        Yii::$container->set(
            'kartik\widgets\Select2',
            UI::getDefaultConfigSelect2()
        );
        Yii::$container->set(
            'kartik\select2\Select2',
            UI::getDefaultConfigSelect2()
        );
        Yii::$container->set(
            'kartik\dialog\Dialog',
            UI::getDefaultConfigDialog()
        );
        Yii::$app->session->open();
        return parent::beforeAction($action);
    }

    /**
     * Configuración de comportamientos
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'restore' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lista todos registros según el DataProvider.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchObject = \Yii::createObject($this->searchModel);
        $paramsGet = Yii::$app->request->get();
        if (!empty($paramsGet)) {
            $searchObject->setAttributes($paramsGet);
        }
        $this->beforeSearchIndex($searchObject);
        $dataProvider = $searchObject->search(Yii::$app->request->queryParams);
        $viewFile = $this->getViewPath() . DIRECTORY_SEPARATOR . C::INDEX . '.php';

        if (is_file($viewFile)) {
            $viewFile = 'index';
        } else {
            $viewFile = '@app/views/common/index';
        }
        $params = [
            'searchModel' => $searchObject,
            'dataProvider' => $dataProvider,
        ];
        $this->beforeRenderIndex($viewFile, $params);
        return $this->render($viewFile, $params);
    }

    /**
     * Permite visualziar los datos de un solo registro.
     *
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Permite visualizar el formulario para la creación de un registro
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new $this->model();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Message::setMessage(Message::TYPE_SUCCESS, Yii::t('app', 'Se creó con éxito.'));

                return $this->redirect(['view', 'id' => $model->primaryKey]);
            } else {
                Message::setMessage(Message::TYPE_DANGER, Yii::t('app', 'Ocurrió un error al intentar crear.'));
            }
        }

        return $this->render(
            'create',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Permite visualizar el formulario de actualización de un registro
     * o realizar la validación y modificación del registro.
     *
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Message::setMessage(Message::TYPE_SUCCESS, Yii::t('app', 'Se editó con éxito.'));

                return $this->redirect(['view', 'id' => $model->primaryKey]);
            } else {
                Message::setMessage(Message::TYPE_DANGER, Yii::t('app', 'Ocurrió un error al intentar editar.'));
            }
        }

        return $this->render(
            'update',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Permite realizar el borrado lógico del registro (activo)
     *
     * @param integer $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $modelObject = \Yii::createObject($this->model);
        $model = $this->findModel($id);
        $model->{$modelObject::STATUS_COLUMN} = $modelObject::STATUS_INACTIVE;

        if ($model->save(false)) {
            Message::setMessage(Message::TYPE_SUCCESS, Yii::t('app', 'Se eliminó con éxito.'));
        } else {
            Message::setMessage(Message::TYPE_DANGER, Yii::t('app', 'Ocurrió un error al intentar eliminar.'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Permite realizar la restauración del registro (activo)
     *
     * @param integer $id
     * @return Response
     */
    public function actionRestore($id)
    {
        $modelObject = \Yii::createObject($this->model);
        $model = $this->findModel($id);
        $model->{$modelObject::STATUS_COLUMN} = $modelObject::STATUS_ACTIVE;

        if ($model->save(false)) {
            Message::setMessage(Message::TYPE_SUCCESS, Yii::t('app', 'Se restauró con éxito.'));
        } else {
            Message::setMessage(Message::TYPE_DANGER, Yii::t('app', 'Ocurrió un error al intentar restaurar.'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Busca un registro basado en su llave primaria.
     * Si el registro no existe arroja un error HTTP 404.
     *
     * @param integer $id
     * @return Model con el registro.
     * @throws NotFoundHttpException is el registro no es encontrado.
     */
    protected function findModel($id)
    {
        $modelObject = \Yii::createObject($this->model);
        if (($model = $modelObject::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t(
                'app',
                'La página solicitada no existe.'
            ));
        }
    }

    public function getTitleReportExport()
    {
        $valorRetorno = str_replace('-', '_', Yii::$app->controller->id);
        if (!empty($this->model) && strlen(trim($this->model)) > 0) {
            $modelObject = \Yii::createObject($this->model);
            if (property_exists($modelObject, 'titleReportExport') && !empty($modelObject::$titleReportExport)) {
                $valorRetorno = $modelObject::$titleReportExport;
            }
        }
        return $valorRetorno;
    }
}
