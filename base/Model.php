<?php

namespace ticmakers\base\base;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Modelo base
 *
 * @package ticmakers
 * @subpackage base
 * @category Components
 *
 * @property string $titleReportExport Almacena el nombre del reporte generado con kartik-yii2-export
 * @property-read string STATUS_ACTIVE Estado activo según la columna.
 * @property-read string STATUS_INACTIVE Estado inactivo según la columna.
 * @property-read string STATUS_COLUMN Nombre de columna de estado.
 * @property-read string CREATED_AT_COLUMN Nombre de columna de Creado por.
 * @property-read string CREATED_DATE_COLUMN Nombre de columna de Fecha creación.
 * @property-read string UPDATED_AT_COLUMN Nombre de columna de Modificado por.
 * @property-read string UPDATED_DATE_COLUMN Nombre de columna de Fecha modificación.
 *
 * @property \yii\web\User $creadoPor
 * @property \yii\web\User $modificadoPor
 *
 * @author  Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Model extends ActiveRecord
{
    const STATUS_ACTIVE = 'Y';
    const STATUS_INACTIVE = 'N';
    const STATUS_COLUMN = 'active';
    const CREATED_AT_COLUMN = 'created_by';
    const CREATED_DATE_COLUMN = 'created_at';
    const UPDATED_AT_COLUMN = 'updated_by';
    const UPDATED_DATE_COLUMN = 'updated_at';
    const DEFAULT_USER_ID = 1;

    public static $titleReportExport = null;

    /**
     * Configuración inicial.
     *
     * @return null
     */
    public function init()
    {
        parent::init();
        $this->{$this::STATUS_COLUMN} = static::STATUS_ACTIVE;
    }

    /**
     * Configuración de los comportamientos por defecto de la aplicación
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => static::CREATED_AT_COLUMN,
                'updatedByAttribute' => static::UPDATED_AT_COLUMN,
                'defaultValue' => (isset(Yii::$app->params['defaultUserId'])) ? Yii::$app->params['defaultUserId'] : $this::DEFAULT_USER_ID,
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => static::CREATED_DATE_COLUMN,
                'updatedAtAttribute' => static::UPDATED_DATE_COLUMN,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreadoPor()
    {
        return $this->hasOne(
            Yii::$app->user->identityClass,
            [Yii::$app->user->identity->getTableSchema()->primaryKey[0] => static::CREATED_AT_COLUMN]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModificadoPor()
    {
        return $this->hasOne(
            Yii::$app->user->identityClass,
            [Yii::$app->user->identity->getTableSchema()->primaryKey[0] => static::UPDATED_AT_COLUMN]
        );
    }

    /**
     * Método encargado de entregar el nombre de la columna para relaciones
     *
     * @return string
     */
    public static function getNameFromRelations()
    {
        foreach (static::getTableSchema()->columns as $column) {
            if (!$column->isPrimaryKey) {
                return $column->name;
            }
        }
    }

    /**
     * Sobreescribimos para quitar campos de auditoria
     * @return array
     */
    public function fields()
    {
        $fields = parent::fields();

        unset($fields[static::STATUS_COLUMN], $fields[static::CREATED_AT_COLUMN],
        $fields[static::CREATED_DATE_COLUMN],
        $fields[static::UPDATED_AT_COLUMN],
        $fields[static::UPDATED_DATE_COLUMN]);

        return $fields;
    }

    /**
     * Permite establecer la configuración para las acciones de combos dependientes asociadas al modelo
     * 
     * return [
            'cities' => [
                // 'class'=>\kartik\depdrop\DepDropAction::className(), // opcional
                'outputCallback' => function ($selectedId, $params) {
                    $result = [];
                    $allData = static::getData(false, ['country_id' => $selectedId]);
                    foreach ($allData as $item) {
                        $result[] = [
                            'id' => $item->primaryKey,
                            'name' => $item->name
                        ];
                    }
                    return $result;
                },
            ]
        ];
     *
     * @return void
     */
    public function depDropDependencies()
    {
        return [];
    }
}
