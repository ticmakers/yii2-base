<?php

namespace ticmakers\base\base;

use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Controlador base para las CRUD con implementación en ventanas modales
 *
 * @package ticmakers
 * @subpackage base
 * @category Base
 *
 * @property string $searchModel Ruta del modelo para la búsqueda.
 * @property string $model Ruta del modelo principal.
 * @property string $layout PATH del layout utilizado por defecto.
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ControllerModal extends Controller
{

    /**
     * Lista todos registros según el DataProvider.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new $this->searchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Permite visualziar los datos de un solo registro.
     *
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        return $this->renderAjax(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Permite visualizar el formulario para la creación de un registro
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new $this->model();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $res['state'] = 'success';
                $res['message'] = Yii::t('app', 'Se creó con éxito.');
            } else {
                $res['state'] = 'error';
                $res['message'] = \yii\helpers\Html::errorSummary($model);
                $res['error'] = ActiveForm::validate($model);
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $res;
        } else {
            return $this->renderAjax(
                '_form',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Permite visualizar el formulario de actualización de un registro
     * o realizar la validación y modificación del registro.
     *
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $res['state'] = 'success';
                $res['message'] = Yii::t('app', 'Se editó con éxito.');
            } else {
                $res['state'] = 'error';
                $res['message'] = \yii\helpers\Html::errorSummary($model);
                $res['error'] = ActiveForm::validate($model);
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $res;
        } else {
            return $this->renderAjax(
                '_form',
                [
                    'model' => $model,
                ]
            );
        }
    }

}
