<?php

namespace ticmakers\base\base;

use Yii;
use kartik\grid\ActionColumn as ActionColumnBase;
use kartik\grid\GridView;
use mdm\admin\components\Helper;
use ticmakers\base\helpers\Html;

/**
 * ActionColumn personalizado para los CRUDS con modals
 *
 * @package ticmakers
 * @subpackage base
 * @category Components
 *
 * @property string $template Template para los botones
 * @property array $restoreOptions Opciones para el boton de restore
 * @property boolean $isModal Indica si el ActionColumn se usa con ventanas modales
 * @property string $dynaModalId Indica el id para encontrar la modal y el dynagrid para enventos JS en ventanas modales
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ActionColumn extends ActionColumnBase
{

    public $width          = '110px';
    public $template       = '{update} {delete} {restore}';
    public $restoreOptions = [];
    public $isModal        = false;
    public $dynaModalId    = '';

    /**
     * Inicializamos los datos del ActionColumn
     *
     * @return void
     */
    public function init()
    {
        $this->hAlign         = GridView::ALIGN_CENTER;
        $this->deleteOptions  = [
            'data-method'  => 'post',
            'data-confirm' => Yii::t('app',
                                     '¿Está seguro que desea eliminar este elemento?')
        ];
        $this->restoreOptions = [
            'data-method'  => 'post',
            'data-confirm' => Yii::t('app',
                                     '¿Está seguro que desea restaurar este elemento?')
        ];

        parent::init();
    }

    /**
     * Asignar una configuración por defecto al botón basado en su $name (es diferente al método [[initDefaultButton]])
     *
     * @param string $name Nombre del botón escribo en [[template]]
     * @param string $title Título de el botón
     * @param string $icon Icono por defecto del botón
     */
    protected function setDefaultButton($name, $title, $icon)
    {
        if (isset($this->buttons[$name]))
        {
            return;
        }
        $this->buttons[$name] = function ($url, $data) use ($name, $title, $icon)
        {
            $opts      = "{$name}Options";
            $options   = ['title' => $title, 'aria-label' => $title, 'data-pjax' => '0'];
            $className = $data::className();

            if (defined("{$className}::STATUS_COLUMN"))
            {

                if ($name === 'delete' && $data->{$data::STATUS_COLUMN} == $data::STATUS_INACTIVE)
                {
                    return;
                }
                elseif ($name === 'restore' && $data->{$data::STATUS_COLUMN} == $data::STATUS_ACTIVE)
                {
                    return;
                }
            }

            if ($this->isModal && !empty($this->dynaModalId) && $name !== 'delete' && $name !== 'restore')
            {
                $options['onClick'] = "openModalGrid(this, '{$this->dynaModalId}', '{$name}'); return false;";
            }

            $options = array_replace_recursive($options, $this->buttonOptions,
                                               $this->$opts);
            $label   = $this->renderLabel($options, $title,
                                          ['class' => "fa fa-{$icon}"]);
            $link    = Html::a($label, $url, $options);
            if ($this->_isDropdown)
            {
                $options['tabindex'] = '-1';
                return "<li>{$link}</li>\n";
            }
            else
            {
                return $link;
            }
        };
    }

    /**
     * Inicializamos los buttons por defecto
     */
    protected function initDefaultButtons()
    {
        $this->setDefaultButton('view', Yii::t('app', 'Ver'), Html::ICON_EYE);
        $this->setDefaultButton('update', Yii::t('app', 'Editar'),
                                                 Html::ICON_PENCIL);
        $this->setDefaultButton('delete', Yii::t('app', 'Eliminar'),
                                                 Html::ICON_TRASH);
        $this->setDefaultButton('restore', Yii::t('app', 'Restaurar'),
                                                  Html::ICON_REPEAT);
    }

}
