/**
 * Script para el control de las funcionalidades del código generado.
 * 
 * @author Daniel Julian Sanchez <daniel.sanchez@ticmakers.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @author Juan Sebastian Muñoz <juan.munoz@ticmakers.com>
 */
var modalOpen = null

$(function () {
    yii.allowAction = function ($e) {
        var message = $e.data('confirm');
        return message === undefined || yii.confirm(message, $e);
    };
    yii.confirm = function (message, success) {
        let element = $(this)
        bootbox.confirm(message, function (confirmed) {
            if (confirmed) {
                if (element.hasClass('no-validate')) {
                    window.location.href = element.attr('href')
                } else {
                    success();
                }
            }
        });
        return false;
    }
    $('.breadcrumb li:first-child a').prepend("<i class='fa fa-home'></i> ");
    registerTooltipPopover();
    // Set default action to modals
    $('.modal').each((index, element) => {
        $(element).on('show.bs.modal', () => {
            modalOpen = $(element)
            registerTooltipPopover();
        })
    })

    $('.modal .confirm-close').each((index, element) => {
        $(element).on('click', (ev) => {
            ev.preventDefault()
            ev.stopPropagation()
            let message = ($(element).attr('data-message')) ? $(element).attr('data-message') : '¿Está seguro que desea cancelar?'
            bootbox.confirm(message, (confirm) => {
                if (confirm) {
                    modalOpen.modal('hide')
                }
            })
        })
    })

    // Set location
    bootbox.addLocale('yii2-es', {
        OK: 'Aceptar',
        CANCEL: 'Cancelar',
        CONFIRM: 'Aceptar'
    })
    // Set default configuration to bootbox
    bootbox.setDefaults({
        locale: 'yii2-es'
    })

    // Configuración por defecto para los toasr
    toastr.options = {
        "timeOut": "9000"
    }
});

/**
 * Método para mostrar un mensaje de success
 * @param {String} msg
 */
function showSuccessMessage(msg) {
    getMessage('success', msg);
}

/**
 * Función para mostrar un mensaje de error
 * @param {String} msg
 */
function showErrorMessage(msg) {
    getMessage('error', msg);
}


/**
 * Función para mostrar un mensaje de info
 * @param {String} msg
 */
function showInfoMessage(msg) {
    getMessage('info', msg);
}

/**
 * Función para mostrar un mensaje
 * @param {String} type
 * @param {String} message
 */
function getMessage(type, message) {
    toastr.clear();
    toastr[type]('<b>' + message + '</b>');
}

/**
 * Función para usar dentro de los eventos change para combos dependientes
 *
 * @param {Object} padre
 * @param {Object} hijo
 * @param {String} url
 * @param {Object} params
 * @param {Function} callBack
 */
function onChangeCombo(padre, hijo, url, params, callBack) {
    if (!params) {
        params = {
            id: padre.val()
        };
    }

    $.getJSON(url, params,
        function (data) {
            changeCombo(data, hijo, callBack);
        });
}

/**
 * Encargado de rellenar un select con la data indicada
 * @param {Object} data
 * @param {Object} hijo
 * @param {Function} callBack
 */
function changeCombo(data, hijo, callBack) {
    $('select[id$=' + hijo.attr('id') + '] > option').remove();
    hijo.append("<option value=''>" + window.TEXT_EMPTY + "</option>");
    for (var value in data) {
        var name = data[value];
        hijo.append("<option value='" + value + "'>" + name + "</option>");
    }

    if (callBack && typeof (callBack) == 'function') {
        callBack(data, hijo);
    }
}

/**
 * Encargado de rellenar un select agrupado con la data indicada
 * @param {Object} data
 * @param {Object} hijo
 * @param {Function} callBack
 */
function changeComboGroup(data, hijo, callBack) {

    $('select[id$=' + hijo.attr('id') + '] > option').remove();
    hijo.append("<option value=''>" + window.TEXT_EMPTY + "</option>");
    for (var label in data) {
        var dataItems = data[label];
        hijo.append("<optgroup  label='" + label + "'>");
        for (var value in dataItems) {
            var name = dataItems[value];
            hijo.append("<option value='" + value + "'>" + name + "</option>");
        }
        hijo.append("</optgroup>");
    }

    if (callBack && typeof (callBack) == 'function') {
        callBack(data, hijo);
    }
}

/**
 * Función encargada de retornar el valor de un parámetro url
 * @param {String} name
 * @returns {String}
 */
function getParamUrl(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(window.location);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/**
 * Función encargada de abrir las modales de CRUD con opción de modal
 * @param {Object} button
 * @param {String} dynaModalId
 * @param {String} template
 */
function openModalGrid(button, dynaModalId, template, callBack) {
    let modalId = '#' + dynaModalId + '-modal';

    if (template == 'create') {
        $(modalId).find('.modal-title').html('Nuevo');
        $('#btn-send-back').show()
    } else if (template == 'update') {
        $(modalId).find('.modal-title').html('Editar');
        $('#btn-send-back').hide()
    }

    if (template == 'view') {
        $(modalId).find('.modal-title').html('Ver');
        $(modalId).find('.modal-footer').hide();
    } else {
        $(modalId).find('.modal-footer').show();
    }

    $(modalId).find('.modal-body').load($(button).attr('href'), function () {
        $(modalId).modal('show');
        registerTooltipPopover();
        $(modalId).find('.modal-body form').on('beforeSubmit', function (event, jqXHR, settings) {
            let form = $(this)
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: new FormData(this),
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                beforeSend: function () {
                    $(modalId).find('button[type="submit"]').prop('disabled', true);
                },
                success: function (data) {
                    if (data.state == 'success') {
                        $(modalId).modal('hide');
                        $(this).off('beforeSubmit');
                        showSuccessMessage(data.message);
                        if (typeof callBack == 'function') {
                            callBack(data);
                        }
                        $.pjax.reload({
                            container: '#dynagrid-' + dynaModalId + '-pjax'
                        })
                    } else {
                        showErrorMessage(data.message);
                    }
                    if (template == 'create' && form.attr('data-back') == 'true') {
                        setTimeout(function () {
                            openModalGrid(button, dynaModalId, template)
                        }, 1000)
                    }
                },
                complete: function () {
                    $(modalId).find('button[type="submit"]').prop('disabled', false);
                }
            })
            return false
        });

        /**
         * Refresco de evento para los formulario con controles tipo yii2-number
         */
        $(modalId).find('.modal-body form').on('afterValidate', function (e) {
            $('.number-input').each((index, element) => {
                $(element).trigger('change')
            })
        })
    });
}

/**
 * Dispara un mensaje de confirmación justo antes de cerra una ventana modal
 */
function closeModalWithMessage(btn) {
    let message = ($(btn).attr('data-message')) ? $(btn).attr('data-message') : '¿Está seguro que desea cancelar?'
    bootbox.confirm(message, (confirm) => {
        if (confirm) {
            modalOpen.modal('hide')
        }
    })
}

/**
 * Registro de tooltips y popovers
 */
function registerTooltipPopover() {
    setTimeout(() => {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    }, 500);
}

/**
 * Añadida validación para campos en los cuales existe controles del tipo yii2-number
 */
$(document).ajaxComplete(function () {
    $(this).on('change', '.number-input', function (e) {
        e.stopPropagation();
        let inputOriginal = $(this);
        let id = `#${inputOriginal.prop('id')}-disp`;
        setTimeout(() => {
            let inputDisplaying = $(id);
            inputDisplaying.removeClass('is-valid').removeClass('is-invalid')
            if ((inputOriginal.attr('aria-invalid') && !inputOriginal.hasClass('is-valid')) || (typeof inputOriginal.attr('aria-invalid') == 'undefined')) {
                inputDisplaying.addClass('is-invalid')
            } else {
                inputDisplaying.addClass('is-valid')
            }
        }, 300);
    })
});