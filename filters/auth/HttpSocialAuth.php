<?php

namespace ticmakers\base\filters\auth;

use Yii;
use yii\filters\auth\AuthMethod;
use ticmakers\base\models\User;
use ticmakers\base\models\RedesSocialesUsuario;
use yii\base\DynamicModel;

/**
 * Implementación de autenticacion normal y por redes sociales
 *
 * @package ticmakers
 * @subpackage filters/auth
 * @category Filters
 *
 * @property string $realm
 *
 * @author Kevin Guzmán <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class HttpSocialAuth extends AuthMethod
{

    public $realm = 'api';

    /**
     * Proceso de validación
     */
    public function authenticate($user, $request, $response)
    {
        $infoUsuario = null;
        /* Validacion de datos */
        $model       = new DynamicModel([
            'user_name', 'red_social', 'id_usuario_red_social', 'respuesta', 'email'
        ]);
        $model->addRule(['user_name', 'red_social', 'id_usuario_red_social', 'respuesta',
                    'email'], 'required')
                ->addRule(['email'], 'email')
                ->addRule(['id_usuario_red_social'], 'string', ['max' => 128])
                ->addRule(['red_social'], 'in',
                          ['allowArray' => true, 'range' => ['FAB', 'GOP']]);

        if ($model->load($request->post(), '') && $model->validate())
        {
            $infoUsuario = User::findByName($model->email);
            if ($infoUsuario !== null)
            {
                if ($this->existeIdRedesSociales($model->id_usuario_red_social,
                                                 $model->red_social,
                                                 $infoUsuario))
                {
                    $infoUsuario->generarAccessToken();
                    $user->switchIdentity($infoUsuario);
                }
            }
            else
            {
                $infoUsuario = $this->crearUsuario($request->post());
                $infoUsuario->generarAccessToken();
                $user->switchIdentity($infoUsuario);
            }
        }
        else
        {
            throw new \yii\web\HttpException(401,
                                             Yii::t('app',
                                                    'Credenciales de autenticación invalidas.'));
        }
        return $infoUsuario;
    }

    /**
     * Valida que el token pasado es valido en las redes sociales registradas
     *
     * @param string $tokenRedSocial Identificador de la red social
     * @param string $idRedSocial tipo de red social
     * @param User $infoUsuario Información del usuario
     * @return boolean
     */
    public function existeIdRedesSociales($tokenRedSocial, $idRedSocial,
                                          $infoUsuario)
    {
        $encontrado    = false;
        $redesSociales = $infoUsuario->redesSocialesUsuario;
        if (count($redesSociales) > 0)
        {
            foreach ($redesSociales as $redSocial)
            {
                if ($redSocial->id_usuario_red_social === $tokenRedSocial && $redSocial->red_social === $idRedSocial)
                {
                    $encontrado = true;
                }
            }
        }
        return $encontrado;
    }

    /**
     * Realiza la creación de un usuario y una red social segun los datos pasados
     *
     * @param array $parametrosPost parametros de la petición post
     * @return User
     */
    public function crearUsuario($parametrosPost)
    {
        /* Creación de usuario */
        $infoUsuario = new User;
        $infoUsuario->load($parametrosPost, '');
        $infoUsuario->save(false);
        $this->crearRedSocial($infoUsuario->primaryKey, $parametrosPost);
        /* Retorno usuario */
        return $infoUsuario;
    }

    /**
     * Realiza la creación de la red social del usuario
     *
     * @param User $infoUsuario
     * @return RedesSocialesUsuario
     */
    public function crearRedSocial($idUsuario, $parametrosPost)
    {
        /* Creación de red social */
        $parametrosPost = array_merge($parametrosPost,
                                      [
            'user_id' => $idUsuario
        ]);
        $infoRedSocial  = new RedesSocialesUsuario;
        $infoRedSocial->load($parametrosPost, '');
        $infoRedSocial->save(false);
        /* Retorno Red Social */
        return $infoRedSocial;
    }

}
