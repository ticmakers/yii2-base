<?php

namespace ticmakers\base\rest\actions;

use Yii;

/**
 * Pemite registrar varios elementos del modelo suministrado
 *
 * @package ticmakers
 * @subpackage rest/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright (c) 2018, TIC Makers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class CreateAllAction extends \yii\rest\Action
{

    /**
     * Modelos para la creación
     * @var array
     */
    public $models;

    /**
     * Permite establecer si se debe registrar o no los datos si alguna de las instacias enviadas falla
     * @var array
     */
    public $skipErrors;

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey;

    /**
     * Runs the action
     *
     * @return string result content
     */
    public function run()
    {

        if ($this->checkAccess)
        {
            call_user_func($this->checkAccess, $this->id);
        }

        $this->models     = Yii::$app->request->post('data', []);
        $this->skipErrors = Yii::$app->request->post('skipErrors', false);

        $inserts = [];
        $updates = [];
        $errors  = [];

        $transaction = Yii::$app->db->beginTransaction();

        try
        {
            foreach ($this->models as $model)
            {

                $modelFind = Yii::createObject($this->modelClass);

                $modelOld = $modelFind->find()->where(['activo' => 'S', $this->primaryKey => $model[$this->primaryKey]])->one();

                if ($modelOld && isset($model[$this->primaryKey]))
                {
                    $modelInsert = clone $modelOld;
                }
                else
                {
                    $modelInsert = Yii::createObject($this->modelClass);
                    if (isset($model[$this->primaryKey]) && !empty($model[$this->primaryKey]))
                    {
                        $modelInsert->{$this->primaryKey} = $model[$this->primaryKey];
                    }
                }

                $modelInsert->setAttributes($model);


                if ($modelInsert->save())
                {
                    if ($modelOld)
                    {
                        $updates[] = $modelInsert->{$this->primaryKey};
                    }
                    else
                    {
                        $inserts[] = $modelInsert->{$this->primaryKey};
                    }
                }
                else
                {
                    $errors[$model[$this->primaryKey]] = $modelInsert->getErrors();
                }
            }

            if (!$this->skipErrors && count($errors) > 0)
            {
                $inserts = [];
                $updates = [];
                $transaction->rollBack();
            }
            else
            {
                $transaction->commit();
            }
        }
        catch (Exception $exc)
        {
            $transaction->rollBack();
            echo $exc->getTraceAsString();
        }

        return [
            'updates' => $updates,
            'inserts' => $inserts,
            'errors'  => $errors
        ];
    }

}
