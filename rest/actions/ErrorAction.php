<?php

namespace ticmakers\base\rest\actions;

use Yii;
use yii\base\Exception;
use yii\base\UserException;

/**
 * Clase encargada de presentar y manipular la información de los errores y excepciones generadas en las api rest
 *
 * @package ticmakers
 * @subpackage rest/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ErrorAction extends \yii\web\ErrorAction
{

    /**
     * Runs the action
     *
     * @return string result content
     */
    public function run()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null)
        {
            // action has been invoked not from error handler, but by direct route, so we display '404 Not Found'
            $exception = new HttpException(404, Yii::t('yii', 'Page not found.'));
        }

        if ($exception instanceof HttpException)
        {
            $code = $exception->statusCode;
        }
        else
        {
            $code = $exception->getCode();
        }
        if ($exception instanceof Exception)
        {
            $name = $exception->getName();
        }
        else
        {
            $name = $this->defaultName ?: Yii::t('yii', 'Error');
        }
        if ($code)
        {
            $name .= " (#$code)";
        }

        if ($exception instanceof UserException)
        {
            $message = $exception->getMessage();
        }
        else
        {
            $message = $this->defaultMessage ?: Yii::t('yii',
                                                       'An internal server error occurred.');
        }

        $datos = [
            'name'    => $name,
            'message' => $message,
            'code'    => $exception->getCode(),
            'status'  => $exception->statusCode,
            'type'    => 'yii\web\NotFoundHttpException',
        ];

        return $datos;
    }

}
