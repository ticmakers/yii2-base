<?php

namespace ticmakers\base\rest\actions;

use Yii;

/**
 * Pemite obtener los listados correspondientes para realizar inserción, actualización y elminación de registros para el modelo actual
 *
 * @package ticmakers
 * @subpackage rest/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class SyncActionMultiple extends \yii\rest\Action
{

    /**
     * Modelo para las busquedas
     * @var string
     */
    public $searchModel;

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey;

    /**
     * Permite definir la estructura para procesar los datos con campos adicionales
     * @var array
     */
    public $processStructure;

    /**
     * Runs the action
     *
     * @return string result content
     */
    public function run()
    {
        if ($this->checkAccess)
        {
            call_user_func($this->checkAccess, $this->id);
        }

        $searchModel             = Yii::createObject($this->searchModel);
        $searchModel->primaryKey = $this->primaryKey;
        $searchModel->perPage    = 0;
        $dataProviderInsert      = $searchModel->search(Yii::$app->request->queryParams,
                                                        '');
        $dataProviderInsert->prepare();

        $datosInsertar  = $dataProviderInsert->models;
        $llavesInsertar = array_keys(\yii\helpers\ArrayHelper::map($datosInsertar,
                                                                   $this->primaryKey,
                                                                   $this->primaryKey));

        $dataProviderDelete = $searchModel->searchDelete(Yii::$app->request->queryParams,
                                                         '');
        $dataProviderDelete->prepare();

        $searchModel->notIn = $llavesInsertar;
        $dataProviderUpdate = $searchModel->searchUpdate(Yii::$app->request->queryParams,
                                                         '');
        $dataProviderUpdate->prepare();
        $datosActualizar    = [];



        $datosPorProcesar = [];
        foreach ($dataProviderUpdate->models as $model)
        {
            $datosPorProcesar[] = $model->toArray();
        }

        $this->procesarDatos($datosActualizar, $datosPorProcesar,
                             $this->processStructure);


        $datosEliminar = [];

        foreach ($dataProviderDelete->models as $model)
        {
            $datosEliminar[] = $model->toArray([$this->primaryKey]);
        }

        return [
            'inserts' => $datosInsertar,
            'updates' => $datosActualizar,
            'deletes' => $datosEliminar
        ];
    }

    private function procesarDatos(&$datosProcesados, $datos, $estructura)
    {
        if (!empty($datos))
        {
            foreach ($datos as $datoProcesar)
            {
                $tabla         = $estructura['tabla'];
                $llavePrimaria = $estructura['llavePrimaria'];
                $relaciones    = $estructura['relaciones'] ?? [];
                $campos        = $estructura['campos'] ?? [];

                $estructuraCampos = [];
                foreach ($campos as $campo)
                {
                    $estructuraCampos[$campo] = $datoProcesar[$campo];
                }

                $datosProcesados[$tabla][] = [
                    'set'   => $estructuraCampos,
                    'where' => [$llavePrimaria => $datoProcesar[$llavePrimaria]]
                ];

                if (!empty($relaciones))
                {
                    foreach ($relaciones as $relacion)
                    {

                        $this->procesarDatos($datosProcesados,
                                             $datoProcesar[$relacion['llaveDatos']],
                                             $relacion);
                    }
                }
            }
        }

        return $datosProcesados;
    }

}
