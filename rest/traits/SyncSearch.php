<?php

namespace ticmakers\base\rest\traits;

use yii\data\ActiveDataProvider;

/**
 * SyncSearch
 *
 * @package ticmakers/rest
 * @subpackage traits
 * @category Traits
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
trait SyncSearch
{

    public $primaryKey;

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchUpdate($params)
    {
        if (isset($params['perPage']))
        {
            $this->perPage = $params['perPage'];
        }

        $query = self::find();
        $alias = '';

        if (method_exists($this, 'queryUpdate'))
        {
            $this->queryUpdate($query);
            if (is_array($query->from))
            {
                $alias = array_keys($query->from)[0] . '.';
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'defaultPageSize' => $this->perPage,
            ],
        ]);

        unset($params['notIn']);

        $this->setAttributes($params);

        $this->ultimaFecha        = str_replace('_', ' ', $this->ultimaFecha);
        $query->andFilterWhere(['>', $alias . 'fecha_modificacion', $this->ultimaFecha])
                ->andFilterWhere(['not in', $alias . $this->primaryKey, $this->notIn])
                ->andFilterWhere(['ilike', $alias . 'activo', 'S']);
        $dataProvider->pagination = false;

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDelete($params)
    {
        if (isset($params['notIn']))
        {
            $params['notIn'] = \yii\helpers\Json::decode($params['notIn']);
            $this->notIn     = is_array($params['notIn']) ? array_values($params['notIn']) : [
                    ];
        }

        $query = self::find();
        $alias = '';

        if (method_exists($this, 'queryDelete'))
        {
            $this->queryDelete($query);
            if (is_array($query->from))
            {
                $alias = array_keys($query->from)[0] . '.';
            }
        }

        $query->select(["{$alias}{$this->primaryKey}"]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'defaultPageSize' => $this->perPage,
            ],
        ]);

        $this->setAttributes($params);

        if (empty($this->ultimaFecha))
        {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->andWhere('0=1');
        }
        else
        {
            $this->ultimaFecha = str_replace('_', ' ', $this->ultimaFecha);
            $query->andFilterWhere(['>', "{$alias}fecha_modificacion", $this->ultimaFecha]);

            $query->andFilterWhere(['in', "{$alias}{$this->primaryKey}", $this->notIn])
                    ->andFilterWhere(['ilike', $alias . "activo",
                        'N']);

            $dataProvider->pagination = false;
        }

        return $dataProvider;
    }

}
