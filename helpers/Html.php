<?php

namespace ticmakers\base\helpers;

use ticmakers\base\helpers\Constants as C;
use kartik\helpers\Html as HtmlBase;

/**
 * Esta Clase muestra contenidos HTML con bootstrap
 * @package ticmakers
 * @subpackage helpers
 * @category Helpers
 *
 * @author  Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Html extends HtmlBase
{

    /*
     * Iconos FontAwesome
     */

    const ICON_HOME = 'home';
    const ICON_DASHBOARD = 'dashboard';
    const ICON_ENVELOPE = 'envelope';
    const ICON_LOCK = 'lock';
    const ICON_UNLOCK = 'unlock';
    const ICON_OK = 'check';
    const ICON_REMOVE = 'times';
    const ICON_INFO_SIGN = 'info';
    const ICON_DOWNLOAD = 'download';
    const ICON_SEARCH = 'search';
    const ICON_PLUS = 'plus';
    const ICON_EYE = 'eye';
    const ICON_PENCIL = 'edit';
    const ICON_TRASH = 'trash';
    const ICON_REFRESH = 'refresh';
    const ICON_ANGLE_LEFT = 'angle-left';
    const ICON_USER = 'user';
    const ICON_USER_PLUS = 'user-plus';
    const ICON_USERS = 'users';
    const ICON_FILE_CODE_O = 'file-code-o';
    const ICON_COGS = 'cogs';
    const ICON_ARROW_RIGHT = 'arrow-right';
    const ICON_MONEY = 'money';
    const ICON_FONT = 'font';
    const ICON_BAR_CHART = 'bar-chart';
    const ICON_LINE_CHART = 'line-chart';
    const ICON_FILE = 'file';
    const ICON_CALENDAR = 'calendar';
    const ICON_CALENDAR_TIMES_O = 'calendar-times-o';
    const ICON_BRIEFCASE = 'briefcase';
    const ICON_TASKS = 'tasks';
    const ICON_CHECK_SQUARE = 'check';
    const ICON_FILE_TEXT = 'file-text';
    const ICON_LIST = 'list';
    const ICON_LIST_ALT = 'list-alt';
    const ICON_WECHAT = 'wechat';
    const ICON_DOLLAR = 'dollar';
    const ICON_QUESTION = 'question';
    const ICON_FILE_PDF_O = 'file-pdf-o';
    const ICON_BUILDING = 'building';
    const ICON_MALE = 'male';
    const ICON_SIGN_IN = 'sign-in';
    const ICON_NEWSPAPER_O = 'newspaper-o';
    const ICON_EXCHANGE = 'exchange';
    const ICON_MEDKIT = 'medkit';
    const ICON_USER_MD = 'user-md';
    const ICON_STETHOSCOPE = 'stethoscope';
    const ICON_MAP_MARKER = 'map-marker';
    const ICON_THUMBS_O_UP = 'thumbs-o-up';
    const ICON_BOOK = 'book';
    const ICON_LIST_OL = 'list-ol';
    const ICON_RANDOM = 'random';
    const ICON_YOUTUBE_PLAY = 'youtube-play';
    const ICON_HAND_POINTER_O = 'hand-pointer-o';
    const ICON_CERTIFICATE = 'certificate';
    const ICON_REPEAT = 'redo';
    const ICON_WRENCH = 'wrench';
    const ICON_CLOCK = 'clock-o';
    const ICON_SCISSORS = 'scissors';
    const ICON_MAP = 'map';
    const ICON_CAR = 'car';
    const ICON_TREE = 'tree';
    const ICON_USER_O = 'user-o';
    const ICON_COPY = 'copy';
    const ICON_ADDRESS_BOOK_O = 'address-book-o';
    const ICON_CREDIT_CARD = 'credit-card';
    /* --- Aqui se colocaran los iconos que se vallan necesitando --- */

    /**
     * Entrega un Icono de FontAwesome
     * @param string $icon
     * @param array $options
     * @return string
     */
    public static function iconFontAwesome($icon, $options = [])
    {
        return parent::icon($icon, $options, 'fa fa-', 'i');
    }

    /**
     * Permite obtener el identificador para un atributo de un modelo
     *
     * @param Model $model
     * @param string $attribute
     * @param string $prefix
     * @return string
     */
    public static function activeInputId($model, $attribute, $prefix = null)
    {
        if ($prefix === null) {
            $prefix = C::PREFIX_DEFAULT;
        }
        return $prefix ;//. HtmlBase::getInputId($model, $attribute);
    }
}
