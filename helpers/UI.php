<?php

namespace ticmakers\base\helpers;

use Yii;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use yii\web\JsExpression;

/**
 * Clase para la creación de elementos de la Interfaz del usuario
 * que se utilizan de manera constante
 *
 * @package ticmakers
 * @subpackage helpers
 * @category Helpers
 *
 * @author  Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class UI
{

    /**
     * Retorna un botón en especifico para la búsqueda de datos de los Dynagrids
     * @param string $label Nombre del botón
     * @param string $modalControl Identificador de la modal
     * @param array $options Opciones HTML
     *
     * @return string
     */
    public static function btnSearch(
        $label = null,
        $modalControl = '#search-modal',
        $options = []
    ) {
        $icon = Html::iconFontAwesome(Html::ICON_SEARCH);
        $label = is_null($label) ? Yii::t('app', 'Búsqueda') : $label;

        if (!isset($options['data'])) {
            $options['data'] = [
                'toggle' => 'modal',
                'target' => $modalControl,
            ];
        }

        if (!isset($options['id'])) {
            $options['id'] = 'search-button';
        }

        $options['class'] = 'btn btn-info';
        $options['title'] = $label;

        return Html::button($icon . ' ' . $label, $options);
    }

    /**
     * Entrega el Botón para Refrescar el grid
     * @param string $label Nombre del botón
     * @param  string|array $url Url de la accion en Controller
     * @param  array $options Opciones Html
     * @return string   Button
     */
    public static function btnRefresh(
        $label = null,
        $url = ['index'],
        $options = []
    ) {
        $icon = Html::iconFontAwesome(Html::ICON_REPEAT);
        $label = is_null($label) ? Yii::t('app', 'Refrescar') : $label;

        $options['id'] = isset($options['id']) ? $options['id'] : 'refresh';
        $options['class'] = 'btn btn-outline-secondary';
        $options['data-pjax'] = true;
        $options['title'] = $label;

        return Html::a($icon . ' ' . $label, $url, $options);
    }

    /**
     * Entrega el Botón para Crear Registro
     * @param string $label Nombre del botón
     * @param  string|array $url Url de la accion en Controller
     * @param  array $options Opciones Html
     * @return string   Button
     */
    public static function btnNew(
        $label = null,
        $url = ['create'],
        $options = []
    ) {
        if (!Usuario::checkRouteFromUrl($url)) {
            return;
        }

        $icon = Html::iconFontAwesome(Html::ICON_PLUS);
        $label = is_null($label) ? Yii::t('app', 'Nuevo') : $label;

        $options['id'] = isset($options['id']) ? $options['id'] : 'create';
        $options['class'] = 'btn btn-success';
        $options['title'] = $label;
        if (!isset($options['data-pjax'])) {
            $options['data-pjax'] = 0;
        }

        return Html::a($icon . ' ' . $label, $url, $options);
    }

    /**
     * Entrega el Botón para Actualizar Registro
     * @param string $label Nombre del botón
     * @param  string|array $url Url de la accion en Controller
     * @param  array $options Opciones Html
     * @return string   Button
     */
    public static function btnUpdate($label = null, $url, $options = [])
    {
        if (!Usuario::checkRouteFromUrl($url)) {
            return;
        }

        $icon = Html::iconFontAwesome(Html::ICON_PENCIL);
        $label = is_null($label) ? Yii::t('app', 'Editar') : $label;

        return Html::a(
            $icon . ' ' . $label,
            $url,
            [
                'id' => isset($options['id']) ? $options['id'] : 'update',
                'class' => 'btn btn-primary',
            ]
        );
    }

    /**
     * Entrega el Botón para Eliminar Registro
     * @param string $label Nombre del botón
     * @param  string|array $url Url de la accion en Controller
     * @param  array $options Opciones Html
     * @return string   Button
     */
    public static function btnDelete($label = null, $url, $options = [])
    {
        if (!Usuario::checkRouteFromUrl($url)) {
            return;
        }

        $icon = Html::iconFontAwesome(Html::ICON_TRASH);
        $label = is_null($label) ? Yii::t('app', 'Eliminar') : $label;

        return Html::a(
            $icon . ' ' . $label,
            $url,
            [
                'id' => isset($options['id']) ? $options['id'] : 'delete',
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t(
                        'app',
                        '¿Está seguro de eliminar este registro?'
                    ),
                    'method' => 'post',
                ],
            ]
        );
    }

    /**
     * Encargado de entregar el botón a usar en las  para agregar
     * @param string $label Nombre del botón
     * @param  array $options Opciones Html
     * @return string
     */
    public static function btnNewLine($label = null, $options = [])
    {
        $icon = Html::iconFontAwesome(Html::ICON_PLUS);
        $label = is_null($label) ? Yii::t('app', 'Nuevo') : $label;
        $options['class'] = 'btn-success';
        $options['type'] = 'button';
        $options['data-toggle'] = 'tooltip';
        $options['title'] = $label;

        return Html::button("{$icon} {$label}", $options);
    }

    /**
     * Encargado de entregar el botón a usar en las  para agregar
     * @param string $label Nombre del botón
     * @param  array $options Opciones Html
     * @return string
     */
    public static function btnDeleteLine($label = null, $options = [])
    {
        $icon = Html::iconFontAwesome(Html::ICON_TRASH);
        $label = is_null($label) ? Yii::t('app', 'Eliminar') : $label;
        $options['class'] = 'btn btn-danger';
        $options['type'] = 'button';
        $options['data-toggle'] = 'tooltip';
        $options['title'] = $label;

        return Html::button("{$icon} {$label}", $options);
    }

    /**
     * Entrega el Botón para Enviar Registro
     * @param  string  $label Label del Botón
     * @param  array  $options Opciones html del Botón
     * @return string   Button
     */
    public static function btnSend($label = null, $options = [])
    {
        $options['id'] = isset($options['id']) ? $options['id'] : 'send';
        $label = is_null($label) ? Yii::t('app', 'Guardar') : $label;
        $icon = isset($options['icon']) ? $options['icon'] : Html::ICON_OK;
        $label = Html::iconFontAwesome($icon) . ' ' . $label;

        if (isset($options['class'])) {
            $options['class'] .= ' btn btn-primary';
        } else {
            $options['class'] = ' btn btn-primary';
        }

        return Html::submitButton($label, $options);
    }

    /**
     * Entrega el Botón para hacer reset a un Formulario
     * @param  string  $label Label del Botón
     * @param  array  $options Opciones html del Botón
     * @return string   Button
     */
    public static function btnReset($label = null, $options = [])
    {
        $options['id'] = isset($options['id']) ? $options['id'] : 'send';
        $label = is_null($label) ? Yii::t('app', 'Reiniciar') : $label;
        $icon = isset($options['icon']) ? $options['icon'] : Html::ICON_REFRESH;
        $label = Html::iconFontAwesome($icon) . ' ' . $label;

        if (isset($options['class'])) {
            $options['class'] .= ' btn btn-default';
        } else {
            $options['class'] = ' btn btn-default';
        }

        return Html::resetButton($label, $options);
    }

    /**
     * Entrega el Botón para Cancelar Registro
     * @param  string  $label Label del Botón
     * @param  array  $url Url de la accion en Controller
     * @return string   Button
     */
    public static function btnCancel($label = null, $url = ['index'], $options = [])
    {
        $label = is_null($label) ? Yii::t('app', 'Cancelar') : $label;
        $icon = isset($options['icon']) ? $options['icon'] : Html::ICON_REMOVE;
        $label = Html::iconFontAwesome($icon) . ' ' . $label;
        $btnOptions = array_merge([
            'id' => isset($options['id']) ? $options['id'] : 'cancel',
            'class' => 'btn btn-default',
            'data-confirm' => Yii::t(
                'app',
                '¿Está seguro que desea cancelar?'
            )
        ], $options);
        $btnOptions['class'] .= ' no-validate';
        return Html::a(
            $label,
            $url,
            $btnOptions
        );
    }

    /**
     * Entrega un botón para cerrar una modal con un mensaje de confirmación
     * @return string
     */
    public static function btnCloseModalMessage($label = null, $icon = null, $options = [])
    {
        $icon = is_null($icon) ? Html::ICON_OK : $icon;
        $label = is_null($label) ? Yii::t('app', 'Aceptar') : $label;
        $label = Html::iconFontAwesome($icon) . ' ' . $label;

        if (isset($options['class'])) {
            $options['class'] = 'btn btn-default' . $options['class'];
        } else {
            $options['class'] = 'btn btn-default';
        }
        $options = array_merge([
            'data-message' => Yii::t('app', '¿Está seguro que desea cancelar?'),
            'onclick' => 'closeModalWithMessage(this)'
        ], $options);

        return Html::button($label, $options);
    }

    /**
     * Entrega el Botón para Cerrar modal
     * @param string $label Label del Botón
     * @param string $icon Icono del botón
     * @param array $options Opciones HTML
     * @return string   Button
     */
    public static function btnCloseModal(
        $label = null,
        $icon = null,
        $options = []
    ) {

        $icon = is_null($icon) ? Html::ICON_OK : $icon;
        $label = is_null($label) ? Yii::t('app', 'Aceptar') : $label;
        $label = Html::iconFontAwesome($icon) . ' ' . $label;

        if (isset($options['class'])) {
            $options['class'] = 'btn btn-default ' . $options['class'];
        } else {
            $options['class'] = 'btn btn-default';
        }

        $options['data-dismiss'] = 'modal';

        return Html::button($label, $options);
    }

    /**
     * Entrega la configuración por defecto del kartik\dynagrid\GridView
     *
     * @return array
     */
    public static function getDefaultConfigDynaGrid()
    {
        return [
            'storage' => DynaGrid::TYPE_SESSION,
            'showPersonalize' => true,
            'allowPageSetting' => true,
            'allowThemeSetting' => true,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'toggleButtonGrid' => [
                'label' => Html::iconFontAwesome(Html::ICON_WRENCH),
                'title' => '',
                'data-pjax' => false
            ],
            'deleteConfirmation' => '¿Está seguro de querer eliminar la configuración?',
            'deleteMessage' => 'Eliminar todas las opciones personalizadas...',
        ];
    }

    /**
     * Entrega la configuración por defecto del kartik\grid\GridView
     *
     * @return array
     */
    public static function getDefaultConfigGridView($titleReport = null)
    {
        $titleReport = (!empty($titleReport)) ? $titleReport : str_replace('-', '_', Yii::$app->controller->id);
        return [
            'autoXlFormat' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'resizableColumns' => true,
            'hover' => true,
            'pjax' => true,
            'panelHeadingTemplate' => "<h3>{title-grid}</h3>",
            'panelTemplate' => '
            <div class="panel panel-bordered">
                {panelHeading}
                {panelBefore}
                {items}
                {panelAfter}
                {panelFooter}
            </div>',
            'export' => [
                'label' => '',
                'fontAwesome' => false,
                'showConfirmAlert' => false,
                'target' => GridView::TARGET_BLANK,
                'options' => ['class' => 'btn btn-default'],
                'header' => '<li role="presentation" class="dropdown-header">' . Yii::t(
                    'app',
                    'Exportar Datos'
                ) . '</li>',
                'menuOptions' => [],
            ],
            'exportConfig' => [
                GridView::EXCEL => [
                    'label' => Yii::t('app', 'Excel'),
                    'alertMsg' => Yii::t(
                        'app',
                        'El archivo de Excel sera generado para descargar.'
                    ),
                    'mime' => 'application/application/vnd.ms-excel',
                    'config' => [
                        'worksheet' => Yii::t('app', 'Hoja de Trabajo'),
                        'cssFile' => ''
                    ],
                    'filename' => $titleReport
                ],
                GridView::PDF => [
                    'label' => Yii::t('app', 'PDF'),
                    'alertMsg' => Yii::t(
                        'app',
                        'El archivo de Pdf sera generado para descargar.'
                    ),
                    'config' => [
                        'methods' => [
                            'SetHTMLHeader' => Yii::$app->name . ' - ' . str_replace('_', ' ', ucwords($titleReport)),
                            'SetFooter' => ['{PAGENO}'],
                        ],
                        'options' => [
                            'title' => Yii::t(
                                'app',
                                'Datos Exportados'
                            ),
                            'subject' => Yii::t(
                                'app',
                                'Archivo PDF generado por {name}',
                                ['{name}' => Yii::$app->name]
                            ),
                            'keywords' => Yii::t(
                                'app',
                                'grid, export, pdf'
                            )
                        ]
                    ],
                    'filename' => $titleReport
                ],
            ],
            'krajeeDialogSettings' => [
                'libName' => 'bootbox'
            ]
        ];
    }

    /**
     * Entrega la configuración para el widget kartik\widgets\DatePicker
     * @return array
     */
    public static function getDefaultConfigDatePicker()
    {
        return [
            'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_APPEND,
            'readonly' => true,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'todayBtn' => 'linked',
                'todayHighlight' => true,
            ]
        ];
    }

    /**
     * Entrega la configuración para el widget kartik\widgets\DateTimePicker
     * @return array
     */
    public static function getDefaultConfigDateTimePicker()
    {
        return [
            'type' => \kartik\widgets\DateTimePicker::TYPE_COMPONENT_APPEND,
            'readonly' => true,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd hh:ii:ss',
            ]
        ];
    }

    /**
     * Entrega la configuración para el widget kartik\widgets\Select2
     * @return array
     */
    public static function getDefaultConfigSelect2()
    {
        return [
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ];
    }

    /**
     * Entrega la configuración para el widget kartik\widgets\TimePicker
     * @return array
     */
    public static function getDefaultConfigTimePicker()
    {
        return [
            'pluginOptions' => [
                'minuteStep' => 5,
                'showMeridian' => false,
            ],
            'options' => [
                'readonly' => true,
            ],
        ];
    }

    /**
     * Entrega la configuración para el widget kartik\dialog\Dialog
     * @return array
     */
    public static function getDefaultConfigDialog()
    {
        return [
            'libName' => 'bootbox',
        ];
    }
}
