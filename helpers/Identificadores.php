<?php

namespace ticmakers\base\helpers;

/**
 * Permite la facil administración de los Identificadores de registros base del sistema
 *
 * @package ticmakers
 * @subpackage helpers
 * @category Helpers
 *
 * @author  Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Identificadores
{

}
