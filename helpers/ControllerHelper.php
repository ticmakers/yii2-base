<?php
namespace ticmakers\base\helpers;

use ticmakers\base\helpers\Constants as C;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

class ControllerHelper
{

    /**
     * Permite la normalización de los arreglos de controlladores a ser mapeados en la aplicación
     *
     * @param array $controllersMap
     * @param string $controllerBase
     * @param string $basePathModels
     * @param string $basePathViews
     * @return mixed
     */
    public static function normalizeControllerMap(
        $controllersMap,
        $controllerBase,
        $basePathModels = 'app\models',
        $basePathViews = '@app/views'
    ) {
        foreach ($controllersMap as $key => $value) {

            if (is_array($value)) {
                ArrayHelper::setValue($controllersMap[$key], C::CLASS_NAME, $controllerBase);

                if (isset($value[C::MODEL_NAME]) && !isset($controllersMap[$key][C::MODEL])) {
                    $controllersMap[$key][C::MODEL] = "{$basePathModels}\\base\\{$value[C::MODEL_NAME]}";
                }
                if (isset($value[C::MODEL_NAME]) && !isset($controllersMap[$key][C::SEARCH_MODEL])) {
                    $controllersMap[$key][C::SEARCH_MODEL] = "{$basePathModels}\\searchs\\{$value[C::MODEL_NAME]}";
                }
                if (!isset($controllersMap[$key][C::VIEW_PATH])) {
                    $controllersMap[$key][C::VIEW_PATH] = "{$basePathViews}\\{$key}";
                }

                unset($controllersMap[$key][C::MODEL_NAME]);
            } else if (!is_string($key)) {
                unset($controllersMap[$key]);
                $idController = Inflector::camel2id($value);
                $controllersMap[$idController] = [
                    C::CLASS_NAME => $controllerBase,
                    C::MODEL => "{$basePathModels}\\base\\{$value}",
                    C::SEARCH_MODEL => "{$basePathModels}\\searchs\\{$value}",
                    C::VIEW_PATH => "{$basePathViews}\\{$idController}",
                ];
            } else {
                $controllersMap[$key] = [
                    C::CLASS_NAME => $controllerBase,
                    C::MODEL => "{$basePathModels}\\base\\{$value}",
                    C::SEARCH_MODEL => "{$basePathModels}\\searchs\\{$value}",
                    C::VIEW_PATH => "{$basePathViews}\\{$key}",
                ];
            }
        }
        return $controllersMap;
    }
}
