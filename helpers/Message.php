<?php

namespace ticmakers\base\helpers;

use Yii;

/**
 * Permite la facil administración de los mensajes de operaciones
 *
 * @package ticmakers
 * @subpackage helpers
 * @category Helpers
 *
 * @property-read string $TYPE_SUCCESS Indica el texto que identifica los mensajes "success"
 * @property-read string $TYPE_INFO Indica el texto que identifica los mensajes "info"
 * @property-read string $TYPE_WARNING Indica el texto que identifica los mensajes "warning"
 * @property-read string $TYPE_DANGER Indica el texto que identifica los mensajes "danger"
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Message
{
  const TYPE_SUCCESS = 'success';
  const TYPE_INFO = 'info';
  const TYPE_WARNING = 'warning';
  const TYPE_DANGER = 'error';

  /**
   * Este método añade un mensaje de operación a los flashes de sesión según su tipo
   *
   * @param string $message Mensaje a mostrar
   * @param string $type Tipo de mensaje a mostrar
   */
  public static function setMessage($type, $message)
  {
    Yii::$app->session->setFlash($type, "<b>{$message}</b>");
  }

  /**
   * Este método sera lamado para mostrar el mensaje en alert al momento de borrar un registro
   *
   * @return string Funcion js para validar mensaje de error
   */
  public static function messageDelete()
  {
    $mensaje_success = "<b>" . self::getIcon(Growl::TYPE_SUCCESS) . ' ' . Yii::t(
      'app',
      'Se eliminó con éxito.'
    ) . "</b>";
    $mensaje_error = "<b>" . self::getIcon(Growl::TYPE_DANGER) . ' ' . Yii::t(
      'app',
      'Ocurió un error al intentar eliminar el registro.'
    ) . "</b>";

    return "
            function(link,success,data){
                if(success)
                {
                    showSuccessMessage('{$mensaje_success}');
                }else
                {
                    showErrorMessage('{$mensaje_error}');
                }

            }
        ";
  }

  /**
   * Este método sera lamado para mostrar el mensaje en alert al momento de restaurar un registro
   *
   * @return string Funcion js para validar mensaje de error
   */
  public static function messageRestore()
  {
    $mensaje_success = "<b>" . self::getIcon(Growl::TYPE_SUCCESS) . ' ' . Yii::t(
      'app',
      'Se restauró con éxito.'
    ) . "</b>";
    $mensaje_error = "<b>" . self::getIcon(Growl::TYPE_DANGER) . ' ' . Yii::t(
      'app',
      'Ocurió un error al intentar restaurar el registro.'
    ) . "</b>";

    return "
            function(link,success,data){
                if(success)
                {
                    showSuccessMessage('{$mensaje_success}');
                }else
                {
                    showErrorMessage('{$mensaje_error}');
                }

            }
        ";
  }

  /**
   * Encargado de entregar el icono correspondiente segun el TYPE
   * @param string $type
   * @return string Html con el icono
   */
  private static function getIcon($type)
  {
    $icon = '';
    switch ($type) {
      case self::TYPE_SUCCESS:
        $icon = Html::iconFontAwesome(Html::ICON_OK);
        break;
      case self::TYPE_DANGER:
        $icon = Html::iconFontAwesome(Html::ICON_REMOVE);
        break;
      case self::TYPE_WARNING:
        $icon = Html::iconFontAwesome(Html::ICON_INFO_SIGN);
        break;
    }

    return $icon;
  }


  /**
   * Método encargado de entrega los mensajes enviados desde self::setMessage() en forma de JS
   *
   * @return string
   */
  public static function getMessagesJS()
  {
    $js = '';
    foreach (Yii::$app->session->getAllFlashes() as $type => $message) {

      $js .= "getMessage('{$type}','{$message}')\n";
    }

    return $js;
  }

}
