<?php
namespace ticmakers\base\actions;

use kartik\depdrop\DepDropAction as KartikDepDropAction;
use yii\web\Response;
use Yii;

class DepDropAction extends KartikDepDropAction
{

    /**
     * @inheritdoc
     */
    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->getRequest();
        if (($selected = $request->post($this->parentParam)) && is_array($selected) && (!empty($selected[0]) || $this->allowEmpty)) {
            $params = $request->post($this->otherParam, []);
            return ['output' => $this->getOutput( $selected, $params), 'selected' => $this->getSelected( $selected, $params)];
        }
        return ['output' => '', 'selected' => ''];
    }
}
