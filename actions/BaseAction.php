<?php
namespace ticmakers\base\actions;

use yii\base\Action;
use Yii;

class BaseAction extends Action
{

    /**
     * @todo Documentar
     *
     */
    public $viewPath;

    /**
     * @todo Documentar
     *
     */
    public $layout;

    /**
     * Modulo padre de la instancia actual
     */

    public $module;

    /**
     * Permite establecer si el render se debe realizar por medio de ajax
     *
     * @var boolean
     */
    public $isModal = false;

    /**
     * This method is called right before `run()` is executed.
     * You may override this method to do preparation work for the action run.
     * If the method returns false, it will cancel the action.
     *
     * @return bool whether to run the action.
     */
    protected function beforeRun()
    {
        if (!empty($this->layout)) {
            $this->controller->layout = $this->layout;
        }

        if (!empty($this->viewPath)) {
            $this->controller->viewPath = $this->viewPath;
        }

        $this->module = Yii::$app;

        if ($this->isModal) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }

        return true;
    }
}
