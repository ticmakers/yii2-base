<?php

namespace ticmakers\base\web;

use yii\web\AssetBundle;

/**
 * Esta Clase Administra los Assets para el plugin bootbox.js
 * @package ticmakers
 * @subpackage assets
 * @category Assets
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class BaseAsset extends AssetBundle
{

    /**
     * @var string
     * Source base para el Asset
     */
    public $sourcePath = '@vendor/ticmakers/yii2-base/assets';

    /**
     * @var array
     * Archivos CSS
     */
    public $css = [
        'base.css'
    ];

    /**
     * @var array
     * Archivos JavaScript
     */
    public $js = [
        'base.js'
    ];

    /**
     * @var array
     * Dependencias del Asset
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'ticmakers\base\web\BowerAsset',
    ];

}
