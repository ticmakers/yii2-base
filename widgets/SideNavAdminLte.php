<?php

namespace app\widgets;

use ticmakers\base\helpers\Html;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Este widget extiende yii\bootstrap4\Nav para que se adapte a la plantilla AdminLte
 *
 * En el parametro "icon" usar iconos de FontAwesome
 * Se puede usar el parametro "activeWhitController" para que este activo con
 * cualquier acción que pertenezca al controlador de la ruta del item (por defecto es "true")
 * @package ticmakers
 * @subpackage widgets
 * @category Widgets
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class SideNavAdminLTE extends \yii\bootstrap4\Nav
{

    /**
     * Inicializa el widget
     */
    public function init()
    {
        parent::init();

        Html::addCssClass($this->options, 'sidebar-menu');
        $this->options['data-widget'] = 'tree';
    }

    /**
     * Renders un item del widget
     * @param string|array $item el item que va a render.
     * @return string el resultado del render.
     * @throws InvalidConfigException
     */
    public function renderItem($item)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("La opción 'label' es obligatoria.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $icon = ArrayHelper::getValue($item, 'icon');
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $label = Html::iconFontAwesome($icon) . Html::tag('span', $label);
        $options = ArrayHelper::getValue($item, 'options', []);
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);
        $visible = ArrayHelper::getValue($item, 'visible', true);

        if (!$visible) {
            return '';
        }

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        if ($items !== null) {
            Html::addCssClass($options, ['widget' => 'treeview']);

            $label .= Html::tag(
                'span',
                Html::iconFontAwesome(
                    Html::ICON_ANGLE_LEFT,
                    ['class' => Html::FLOAT_RIGHT]
                ),
                ['class' => 'pull-right-container']
            );

            if (is_array($items)) {
                if ($this->activateItems) {
                    $items = $this->isChildActive($items, $active);
                }

                if ($this->hasChildActive($items)) {
                    Html::addCssClass($options, 'active');
                }

                $items = $this->renderTreeView($items, $item);
            }
        }

        if ($this->activateItems && $active) {
            Html::addCssClass($options, 'active');
        }

        return Html::tag(
            'li',
            Html::a($label, $url, $linkOptions) . $items,
            $options
        );
    }

    /**
     * Sobreescribe método para activarlo tambien cuando
     * este dentro de cualquier accion del controlador
     * @param array $item
     */
    protected function isItemActive($item)
    {
        $activeWhitController = ArrayHelper::getValue(
            $item,
            'activeWhitController',
            true
        );
        $active = parent::isItemActive($item);

        if ($activeWhitController || $active) {
            $route = explode('/', $item['url'][0]);
            if (isset($route[1])) {
                $active = $active || $route[1] === Yii::$app->controller->id;
            }
        }

        return $active;
    }

    /**
     * Indica si algun item hijo esta activo
     * @param array $items Items Hijos
     * @return boolean
     */
    protected function hasChildActive($items)
    {
        foreach ($items as $item) {
            if ($this->isItemActive($item)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Renderiza los items para sub-menus
     * @param array $items items dados
     * @param array $parentItem item Padre
     * @return string Resultado Html
     */
    protected function renderTreeView($items, $parentItem)
    {
        Html::addCssClass($parentItem['options'], 'treeview-menu');
        $content = '';

        foreach ($items as $item) {
            $content .= $this->renderItem($item);
        }

        return Html::tag('ul', $content, $parentItem['options']);
    }

}
