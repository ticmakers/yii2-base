<?php

namespace ticmakers\base\widgets;

use ticmakers\base\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * Este widget extiende yii\bootstrap4\Tabs para que se adapte a la plantilla
 *
 * @package ticmakers
 * @subpackage widgets
 * @category Widgets
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class TabsVertical extends Tabs
{

    public function init()
    {
        parent::init();

        $this->navType .= 'flex-column';
        Html::addCssClass($this->tabContentOptions, 'col-9');
        Html::removeCssClass($this->tabContentOptions, 'pt-20');
    }

    /**
     * Renderiza los items del tab especificado en [[items]].
     *
     * @return string Html con el restultado.
     * @throws InvalidConfigException.
     */
    protected function renderItems()
    {
        $headers = [];
        $panes = [];

        if (!$this->hasActiveTab()) {
            $this->activateFirstVisibleTab();
        }

        foreach ($this->items as $n => $item) {
            if (!ArrayHelper::remove($item, 'visible', true)) {
                continue;
            }
            if (!array_key_exists('label', $item)) {
                throw new InvalidConfigException("The 'label' option is required.");
            }
            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
            $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
            $headerOptions = array_merge(
                $this->headerOptions,
                ArrayHelper::getValue(
                    $item,
                    'headerOptions',
                    []
                )
            );
            $linkOptions = array_merge(
                $this->linkOptions,
                ArrayHelper::getValue(
                    $item,
                    'linkOptions',
                    []
                )
            );

            // Ajustes para el template------------------------
            Html::addCssClass($headerOptions, 'nav-item');
            $headerOptions['role'] = 'presentation';
            Html::addCssClass($linkOptions, 'nav-link');
            //-------------------------------------------

            if (isset($item['items'])) {
                $label .= ' <b class="caret"></b>';
                Html::addCssClass($headerOptions, ['widget' => 'dropdown']);

                if ($this->renderDropdown($n, $item['items'], $panes)) {
                    Html::addCssClass($headerOptions, 'active');
                }

                Html::addCssClass($linkOptions, ['widget' => 'dropdown-toggle']);
                if (!isset($linkOptions['data-toggle'])) {
                    $linkOptions['data-toggle'] = 'dropdown';
                }
                /** @var Widget $dropdownClass */
                $dropdownClass = $this->dropdownClass;
                $header = Html::a($label, "#", $linkOptions) . "\n"
                    . $dropdownClass::widget([
                    'items' => $item['items'],
                    'clientOptions' => false,
                    'view' => $this->getView()
                ]);
            } else {
                $options = array_merge(
                    $this->itemOptions,
                    ArrayHelper::getValue(
                        $item,
                        'options',
                        []
                    )
                );
                $options['id'] = ArrayHelper::getValue(
                    $options,
                    'id',
                    $this->options['id'] . '-tab' . $n
                );
                $options['role'] = 'tabpanel'; //------Ajustes para el template---------
                $linkOptions['aria-controls'] = $options['id']; //------Ajustes para el template---------
                $linkOptions['role'] = 'tab'; //------Ajustes para el template---------

                Html::addCssClass($options, ['widget' => 'tab-pane']);
                if (ArrayHelper::remove($item, 'active')) {
                    Html::addCssClass($options, 'active');
                    //Html::addCssClass($headerOptions, 'active');
                    Html::addCssClass($linkOptions, 'active'); //------Ajustes para el template---------
                }

                if (isset($item['url'])) {
                    $header = Html::a($label, $item['url'], $linkOptions);
                } else {
                    if (!isset($linkOptions['data-toggle'])) {
                        $linkOptions['data-toggle'] = 'tab';
                    }
                    $header = Html::a($label, '#' . $options['id'], $linkOptions);
                }

                if ($this->renderTabContent) {
                    $tag = ArrayHelper::remove($options, 'tag', 'div');
                    $panes[] = Html::tag(
                        $tag,
                        isset($item['content']) ? $item['content'] : '',
                        $options
                    );
                }
            }

            $headers[] = Html::tag('li', $header, $headerOptions);
        }

        $items = Html::tag('div', Html::tag('ul', implode("\n", $headers), $this->options), ['class' => 'col-3']) . $this->renderPanes($panes);

        return Html::tag(
            'div',
            $items,
            ['class' => 'row', 'data-plugin' => 'tabs']
        );
    }

}
