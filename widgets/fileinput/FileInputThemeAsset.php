<?php
namespace ticmakers\base\widgets\fileinput;

use kartik\file\FileInputThemeAsset as KartikFileInputThemeAsset;

class FileInputThemeAsset extends KartikFileInputThemeAsset
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@ticmakers/base/widgets/fileinput';
}
