<?php
namespace ticmakers\base\widgets\fileinput;

use kartik\widgets\FileInput as KartikFileInput;
use yii\helpers\ArrayHelper;

class FileInput extends KartikFileInput
{
    /**
     * @var array the list of inbuilt themes
     */
    protected static $_themes = ['fa', 'fas', 'gly', 'explorer', 'explorer-fa', 'explorer-fas', 'default'];
    /**
     * Registers the asset bundle and locale
     * @throws \yii\base\InvalidConfigException
     */
    public function registerAssetBundle()
    {
        parent::registerAssetBundle();
        $view = $this->getView();
        $theme = ArrayHelper::getValue($this->pluginOptions, 'theme');
        if (!empty($theme) && in_array($theme, self::$_themes)) {
            FileInputThemeAsset::register($view)->addTheme($theme);
        }
    }
}
