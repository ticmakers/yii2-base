<?php


namespace ticmakers\base\widgets\linksorter;


use yii\helpers\Html;

/**
 * Class LinkSorter
 * @package ticmakers
 * @subpackage base\widgets\linksorter
 * @category widgets
 *
 * @author Kevin Daniel Guzmán Delgaddillo
 * @version 0.0.1
 * @since 1.0.0
 */
class LinkSorter extends \yii\widgets\LinkSorter
{
    /**
     * Renders the sort links.
     *
     * @return string the rendering result
     * @throws
     */
    protected function renderSortLinks()
    {
        $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
        return $this->render('main', [
            'attributes' => $attributes,
            'widget' => $this
        ]);
    }
}