<?php
use yii\web\View;
?>
<style>
    /* Style The Dropdown Button */
    .dropdown-control {
        display: flex;
        font-size: .8rem;
        cursor: pointer;
        padding: .6rem 1rem;
        align-items: center;
    }

    .dropdown-control label {
        flex: 1;
        margin: 0px;
        text-align: left;
    }

    .dropdown-control span::before {
        content: '\f107';
        font-family: FontAwesome;
    }

    .dropdown-control .svg-inline--fa {
        text-align: right;
    }

    /* The container <div> - needed to position the dropdown content */
    .dropdown {
        position: relative;
        display: inline-block;
    }

    /* Dropdown Content (Hidden by Default) */
    .dropdown-list {
        display: none;
        position: absolute;
        max-height: 12rem;
        overflow: auto;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    /* Links inside the dropdown */
    .dropdown-list a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    /* Change color of dropdown links on hover */
    .dropdown-list a:hover {background-color: #f1f1f1}

    /* Show the dropdown menu on hover */
    .dropdown:hover .dropdown-list {
        display: block;
    }

    /* Change the background color of the dropdown button when the dropdown content is shown */
    .dropdown:hover .dropdown-control {
        background-color: #218838;
        border-color: #1e7e34;
        color: #fff;
    }
</style>
<div class="dropdown-content">
    <span class="dropdown-title mr-2">Ordenar por: </span>
    <div class="dropdown">
        <div class="dropdown-control btn btn-outline-success">
            <label></label>
            <span></span>
        </div>
        <div class="dropdown-list">
        <?php foreach ($attributes as $name): ?>
            <?= $widget->sort->link($name, $widget->linkOptions) ?>
        <?php endforeach; ?>
        </div>
    </div>
</div>
<?php
$this->registerJs(<<<JS
    function setSelect() {
        $('.dropdown-control').css('min-width', $('.dropdown-list').width());
        let itemsSelected = $('.dropdown-list a.asc, .dropdown-list a.desc');
        let text = [];
        for (let item of itemsSelected) {
            text.push($(item).text());
        }
        text = text.join(', ');
        if (text.length == 0) {
            text = 'Seleccione';
        }
        $('.dropdown-control label').text(text);
    }
    setSelect();
JS
, View::POS_END);
?>