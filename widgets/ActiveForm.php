<?php

namespace ticmakers\base\widgets;

use yii\bootstrap4\ActiveForm as BaseActiveForm;

/**
 * ActiveFom es un widget que construye un formulario HTML interactivo con uno o varios modelos.
 *
 * @package ticmakers
 * @subpackage widgets
 * @category Widgets
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ActiveForm extends BaseActiveForm
{

    /**
     * @var string Using different style themes:
     *
     * + default: Default layout with labels at the top of the fields.
     * + horizontal: Horizontal layout set the labels left to the fields.
     */
    public $layout = 'default';

    /**
     * @var string The error Summary alert class
     */
    public $errorSummaryCssClass = 'error-summary alert alert-danger';

    /**
     * @inheritdoc
     */
    public $errorCssClass = 'is-invalid';

    /**
     * @var string Validation type
     */
    public $validationStateOn = self::VALIDATION_STATE_ON_INPUT;

    /**
     * @inheritdoc
     */
    public $successCssClass = 'is-valid';

    /**
     * @inheritdoc
     */
    public $fieldClass = 'ticmakers\base\widgets\ActiveField';

}
